/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_board.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvoina <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:15:29 by mvoina            #+#    #+#             */
/*   Updated: 2015/12/21 15:42:28 by azapirta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

int			check_board(int nr)
{
	if (nr >= 1 && 10000 >= nr)
		return (1);
	return (0);
}

static int	ft_max_nr(int *board, int nr_of_lines)
{
	int i;
	int max;

	i = 1;
	max = board[0];
	while (i < nr_of_lines)
	{
		if (max < board[i])
			max = board[i];
		i++;
	}
	return (max);
}

static void	put_space(int nr, int max)
{
	int n;

	n = max / 2;
	while (n - nr / 2 + 1 > 0)
	{
		ft_putchar(' ');
		n--;
	}
}

void		print_board(int *board, int nr_of_lines)
{
	int i;
	int nr;
	int max;

	i = 0;
	max = ft_max_nr(board, nr_of_lines);
	while (i < nr_of_lines)
	{
		nr = board[i];
		put_space(nr, max);
		while (nr > 0)
		{
			ft_putchar('|');
			nr--;
		}
		ft_putchar('\n');
		i++;
	}
}

int			*make_board(void)
{
	int *board;

	if ((board = (int*)malloc(1000 * sizeof(int))) == 0)
		return (0);
	return (board);
	return (0);
}
