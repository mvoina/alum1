NAME = alum1

FLAGS = -Wall -Wextra -Werror

SRC =	alum1.c\
	make_board.c\
	check_board.c\
	play_game.c\
	ft_putchar.c\
	ft_putnbr.c\
	ft_memset.c\
	ft_strncat.c\
	ft_strncpy.c\
	ft_strlen.c\
	ft_putstr.c\
	ft_strchr.c\
	ft_atoi.c\
	get_next_line.c\

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME):

	@gcc $(FLAGS) $(SRC) -o $(NAME)

clean:
		@rm -f $(OBJ)

fclean:	clean
		@rm -f $(NAME)

re:	fclean all
