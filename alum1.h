/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alum1.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azapirta <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:34:28 by azapirta          #+#    #+#             */
/*   Updated: 2015/12/21 15:38:05 by azapirta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALUM1_H
# define ALUM1_H
# include <stdlib.h>
# include <fcntl.h>
# include <unistd.h>

# define BUFF_SIZE 32

int		get_next_line(int const fd, char **line);
void	*ft_memset(void *s, int c, size_t n);
char	*ft_strncat(char *dest, const char *src, size_t n);
char	*ft_strncpy(char *dest, const char *src, size_t n);
size_t	ft_strlen(const char *s);
void	ft_putchar(char c);
void	ft_putnbr(int n);
void	ft_putstr(char const *s);
char	*ft_strchr(const char *s, int c);
int		ft_atoi(const char *str);
int		*make_board(void);
void	print_board(int *board, int nr_of_lines);
int		check_board(int nr);
int		play_game(int *board, int size);
int		empty_board(int *board, int size);
int		cpu_pick(int *board, int size);
int		valid_move(int nr, int *board, int size);

#endif
