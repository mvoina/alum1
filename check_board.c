/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_board.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvoina <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:13:33 by mvoina            #+#    #+#             */
/*   Updated: 2015/12/21 15:39:05 by azapirta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

int			empty_board(int *board, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (board[i] == 0)
			i++;
		else
			return (0);
	}
	return (1);
}

static int	board_index(int *board, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (board[i] != 0)
			return (i);
		i++;
	}
	return (0);
}

static int	cpu_return_01(int rest)
{
	if (rest == 0)
		return (3);
	else if (rest == 2)
		return (1);
	return (2);
}

static int	cpu_return_02(int rest)
{
	if (rest == 0)
		return (1);
	else if (rest == 2)
		return (2);
	return (3);
}

int			cpu_pick(int *board, int size)
{
	int rest;
	int index;

	index = board_index(board, size);
	rest = board[index] % 4;
	if (rest == 1)
		return (1);
	else if (board[index + 1] % 4 == 1)
		return (cpu_return_02(rest));
	else if (board[index + 1] % 4 != 1)
		return (cpu_return_01(rest));
	else if (index == 0)
	{
		if (rest == 3)
			return (2);
		else if (rest == 2)
			return (1);
		else if (rest == 0)
			return (3);
	}
	return (0);
}
