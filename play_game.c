/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play_game.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvoina <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:16:21 by mvoina            #+#    #+#             */
/*   Updated: 2015/12/21 15:44:54 by azapirta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static int	who_is_starting(void)
{
	char *line;

	while (1)
	{
		ft_putstr("Who should start?\n1.Player\n2.Computer\n");
		get_next_line(0, &line);
		if (ft_atoi(line) == 1)
		{
			ft_putstr("You chose option >1<  The Player will start\n");
			return (1);
		}
		else if (ft_atoi(line) == 2)
		{
			ft_putstr("\nYou chose option >2<  The Computer will start\n");
			return (2);
		}
		else
			ft_putstr("Enter >1< or >2<\n");
	}
}

int			valid_move(int nr, int *board, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (board[i] == 0)
			i++;
		else if (board[i] - nr >= 0)
		{
			board[i] -= nr;
			return (1);
		}
		else if (board[i] - nr < 0)
			return (0);
	}
	return (-1);
}

static int	cpu_move(int *board, int size)
{
	int nr;
	int valid;

	if (empty_board(board, size))
	{
		ft_putstr("Computer WINS !!!!!!\n");
		return (-1);
	}
	nr = cpu_pick(board, size);
	valid = valid_move(nr, board, size);
	if (valid)
	{
		ft_putstr("\n...........................................");
		ft_putstr("\nComputer's turn\n.....Thinking....\nComputer picked ");
		ft_putnbr(nr);
		ft_putstr(" matches\n");
		return (1);
	}
	return (0);
}

static int	read_player_move(int *board, int size)
{
	char	*line;
	int		nr;

	if (empty_board(board, size))
	{
		ft_putstr("Player WINS !!!!!!\n");
		return (-1);
	}
	ft_putstr("\n...........................................");
	ft_putstr("\nPlayer's turn\n");
	while (1)
	{
		ft_putstr("Enter how many matches you want to pick (min 1, max 3): ");
		get_next_line(0, &line);
		nr = ft_atoi(line);
		if (nr > 0 && nr < 4 && valid_move(nr, board, size))
			return (1);
	}
	return (0);
}

int			play_game(int *board, int size)
{
	int first_move;
	int who;

	first_move = 1;
	while (1)
	{
		if (first_move)
		{
			who = who_is_starting();
			first_move = 0;
		}
		print_board(board, size);
		if (who % 2 == 0)
		{
			if (cpu_move(board, size) == -1)
				return (0);
		}
		else
		{
			if (read_player_move(board, size) == -1)
				return (0);
		}
		who++;
	}
	return (0);
}
