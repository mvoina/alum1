/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alum1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvoina <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:12:58 by mvoina            #+#    #+#             */
/*   Updated: 2015/12/21 15:32:51 by azapirta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static int	read_from_kyb(int *board)
{
	char	*line;
	int		line_nr;

	line_nr = 0;
	while (1)
	{
		get_next_line(0, &line);
		if (line == 0)
		{
			if (line_nr == 0)
				ft_putstr("ERROR\n");
			return (line_nr);
		}
		board[line_nr] = ft_atoi(line);
		if (!check_board(board[line_nr]))
		{
			ft_putstr("ERROR\n");
			return (0);
		}
		line_nr++;
	}
	return (line_nr);
}

static int	read_from_file(char **argv, int *board)
{
	char	*line;
	int		line_nr;
	int		fd;

	line_nr = 0;
	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
	{
		ft_putstr("ERROR\n");
		return (0);
	}
	while (get_next_line(fd, &line) == 1)
	{
		board[line_nr] = ft_atoi(line);
		if (!check_board(board[line_nr]))
		{
			ft_putstr("ERROR\n");
			return (0);
		}
		line_nr++;
	}
	return (line_nr);
}

int			main(int argc, char **argv)
{
	int	*board;
	int	line_nr;

	board = make_board();
	if (argc == 1)
		line_nr = read_from_kyb(board);
	else
		line_nr = read_from_file(argv, board);
	if (line_nr == 0)
		return (0);
	play_game(board, line_nr);
	free(board);
	return (0);
}
